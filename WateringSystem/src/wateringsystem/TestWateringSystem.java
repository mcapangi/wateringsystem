/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wateringsystem;

import java.time.LocalTime;

/**
 *
 * @author Profesor
 */
public class TestWateringSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int h=13,m=30,u=0;
        Senzor s = new Senzor();
        Pump p = new Pump(false);
        Valve vGradina = new Valve(false, "Gradina");
        Valve vCurte = new Valve(false, "Curtea din fata");

        Schedule orarGradina = new Schedule();
        orarGradina.addInterval(new Interval(9, 10));
        orarGradina.addInterval(new Interval(13, 14));
        orarGradina.addInterval(new Interval(18, 20));

        Schedule orarCurte = new Schedule();
        orarCurte.addInterval(new Interval(9, 10));
        orarCurte.addInterval(new Interval(18, 120));

        Controler ctrl = new Controler();

        while (true && m<40) {
            System.out.println(String.format("ora: %d, min: %d ", h,m));
            if (s.ploua()) {
                p.stop();
                ctrl.setValve(vCurte);
                ctrl.closeValve();
                ctrl.setValve(vGradina);
                ctrl.closeValve();
            } else {
                //h = LocalTime.now().getHour();
                //m = LocalTime.now().getMinute();
                m++;
                Boolean isInOrarGradina = orarGradina.contains(h, m);
                Boolean isInOrarCurte = orarCurte.contains(h, m);
                if (isInOrarCurte || isInOrarGradina) {
                    p.start();
                }
                ctrl.setValve(vCurte);
                if (isInOrarCurte) {
                    ctrl.openValve();
                } else {
                    ctrl.closeValve();
                }
                ctrl.setValve(vGradina);
                if (isInOrarGradina) {
                    ctrl.openValve();
                } else {
                    ctrl.closeValve();
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                };
            }
        }
    }

}
