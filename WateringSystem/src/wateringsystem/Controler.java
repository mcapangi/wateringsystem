
package wateringsystem;

public class Controler {
    private Valve valve;
    private Schedule schedule; 

    public Controler(Valve v) {
        this.valve = v;
    }

    public Controler(Schedule schedule) {
        this.schedule = schedule;
    }

    public Controler(Valve valve, Schedule schedule) {
        this.valve = valve;
        this.schedule = schedule;
    }

    public Controler() {
    }
    
    public void openValve()
    {
        if(!valve.getStare())
            valve.setStare(Boolean.TRUE);
    }
    
    public void closeValve()
    {
        if(valve.getStare())
            valve.setStare(Boolean.TRUE);
    }

    public Valve getValve() {
        return valve;
    }

    public void setValve(Valve p) {
        this.valve = p;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }
    
}
