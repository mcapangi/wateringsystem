/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wateringsystem;

/**
 *
 * @author Profesor
 */
public class Pump {

    private Boolean stare;

    public Pump() {
        stare=false;
    }

    public Pump(Boolean stare) {
        this.stare = stare;
    }

    public Boolean getStare() {
        return stare;
    }

    public void setStare(Boolean stare) {
        this.stare = stare;
    }

    void start() {
        if (!stare) {
            stare = true;
            System.out.println("Am pornit pompa.");
        }

    }

    void stop() {
        if (stare) {
            stare = false;
            System.out.println("Am oprit pompa.");
        }

    }

    public String toString() {
        return "Pompa: stare = " + stare;
    }
}
