package wateringsystem;

public class Interval {

    int oraStart, oraStop, minStart, minStop;
    //String nume;

    public Interval(int start, int stop) {
        this.oraStart = start;
        this.oraStop = stop;
        this.minStart = 0;
        this.minStop = 0;
    }

    public Interval() {
        this.oraStart = 9;
        this.oraStop = 10;
        this.minStart = 0;
        this.minStop = 0;
    }

    public Interval(int oraStart) {
        this.oraStart = oraStart;
        this.minStart = 0;
        this.oraStop = oraStart + 1;
        this.minStop = 0;
    }

    public Interval(int oraStart, int oraStop, int minStart, int minStop) {
        this.oraStart = oraStart;
        this.oraStop = oraStop;
        this.minStart = minStart;
        this.minStop = minStop;
    }
    
}
