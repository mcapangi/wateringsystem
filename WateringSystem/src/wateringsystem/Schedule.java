package wateringsystem;

import java.util.ArrayList;

public class Schedule {

    ArrayList<Interval> orar = new ArrayList<Interval>();

    public Schedule(ArrayList<Interval> orar) {
        this.orar = orar;
    }

    public Schedule() {
        orar.add(new Interval());
    }

    void addInterval(Interval i) {
        orar.add(i);
    }

    Boolean contains(int h, int min) {
        for (int i = 0; i < orar.size(); i++) {
            if ((orar.get(i).oraStart <h || orar.get(i).oraStart==h && orar.get(i).minStart <= min )
             && (orar.get(i).oraStop > h || orar.get(i).oraStop == h && orar.get(i).minStop >= min)) {
                return true;
            }
        }
        return false;
    }
}
