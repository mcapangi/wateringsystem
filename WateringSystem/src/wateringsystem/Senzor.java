package wateringsystem;

import java.util.Random;

public class Senzor {

    private int value;
    public final static int prag = 50;//pragul de la care consider ca ploua

    public int read() {
        Random rand = new Random();
        value = rand.nextInt(100);
        return value;
    }

    Boolean ploua() {
        if(this.read() >= prag)
        {
            System.out.println("Ploua!");
            return true;
        }
        else
            return false;
    }

    public String toString() {
        return value + "";
    }
}
